# --- !Ups

INSERT INTO USERS (USER_ID, EMAIL, PASSWORD) VALUES (1L, 'a@b.com', 'secret')

# --- !Downs

DELETE FROM USERS WHERE EMAIL = 'a@b.com'