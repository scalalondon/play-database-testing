# --- !Ups

CREATE TABLE IF NOT EXISTS USERS (
  user_id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(64) DEFAULT NULL,
  password varchar(256) NOT NULL,
  PRIMARY KEY (user_id)
);

# --- !Downs

DROP TABLE IF EXISTS users;

