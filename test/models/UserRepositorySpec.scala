package models

import org.scalatest.{FlatSpec, Matchers}
import org.scalatestplus.play.{OneAppPerSuite, PlaySpec}
import play.api.{GlobalSettings, Play, Application}
import play.api.test.FakeApplication
import play.api.test.Helpers._
import play.api.Logger

/**
 * Test UserRepository.
 */
class UserRepositorySpec extends PlaySpec with OneAppPerSuite {

  val userRepository = new UserRepository

  implicit lazy val globalSettings = Some(new GlobalSettings() {
    override def onStart(app: Application) {
      TestFixture.insert()
    }
  })

  // Override app if you need a FakeApplication with other than
  // default parameters.
  implicit override lazy val app: FakeApplication =
    FakeApplication(
      additionalConfiguration = inMemoryDatabase(name="default"),
      withGlobal = globalSettings
    )

  "UserRepository" must {
    "read existing user from database " in {
      val user = userRepository.findUser("a@b.com")
      user must not be empty
    }
  }

  "UserRepository" must {
    "Find all users in the database database " in {
      val users = userRepository.getAll
      users must not be empty
      users must have size(3)
    }
  }

}

object TestFixture {

  val userRepository = new UserRepository

  def insert() = {
    Logger.info("Inserting Test Fixtures")

    Seq(
      User(Some(2l),"a@b.co.uk","secret"),
      User(Some(3l),"a@b.ie","secret")
    ).foreach(userRepository.save(_))
  }

}