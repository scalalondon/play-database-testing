package models

/**
 * Database Code.
 */
case class User(userId: Option[Long] = None, email: String, password: String)

import anorm.{SQL, SqlQuery}
import org.mindrot.jbcrypt.BCrypt
import play.api.Logger
import play.api.db.DB

/**
 * User Persistence.
 */
class UserRepository {

  val serviceLogger = Logger("UserService")

  import anorm.RowParser
  val userParser: RowParser[User] = {
    import anorm.SqlParser._
    import anorm.~
    long("user_id") ~
      str("email") ~
      str("password") map {
      case userId ~ email ~ password =>
        User(Some(userId), email, password)
    }
  }

  import anorm.ResultSetParser
  val usersParser: ResultSetParser[List[User]] = {
    userParser *
  }

  import play.api.Play.current
  def getAllWithParser: List[User] = DB.withConnection {implicit connection =>
    SQL("select * from USERS").as(usersParser)
  }

  /**
   *
   * @param email
   * @return
   */
  def findUser(email: String): Option[User]  = DB.withConnection {implicit connection =>
    SQL("select * from USERS where email = {email}").
      on("email" -> email ).as(userParser.singleOpt)
  }


  /**
   * Uses Anorm Stream API.
   *
   * @return
   */
  def getAll: List[User]  =  {
    import play.api.Play.current
    val sql: SqlQuery = SQL("select * from USERS order by user_id desc")
    DB.withConnection {
      implicit connection =>
        sql().map ( row =>
          User(row[Option[Long]]("user_id"), row[String]("email"), row[String]("password"))
        ).toList
    }
  }

  /**
   *
   * @param user
   */
  def save(user: User): Boolean = {
    val passwordHash = BCrypt.hashpw(user.password, BCrypt.gensalt());
    serviceLogger.info("Save User:" + user)
    DB.withConnection { implicit connection =>
      val addedRows = SQL(
        """insert into USERS(user_id, email, password)
          | values
          | ({user_id}, {email}, {password})""".stripMargin).on(
          "user_id" -> user.userId,
          "email" -> user.email,
          "password" -> user.password
        )
      addedRows.executeUpdate() == 1
    }
  }

  /**
   *
   * @param user
   * @return
   */
  def update(user: User): Boolean =
    DB.withConnection { implicit connection =>
      val updatedRows = SQL("""update USERS set password = {password},
                                email = {email}
                                where user_id = {user_id} """).on(
          "user_id" -> user.userId,
          "email" -> user.email,
          "password" -> user.password).
        executeUpdate()
      updatedRows == 1
    }

}


