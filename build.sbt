name := """database-testing"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "mysql" % "mysql-connector-java" % "5.1.27",
  "com.typesafe.play.extras" %% "iteratees-extras" % "1.4.0",
  "org.mindrot" % "jbcrypt" % "0.3m",
  "org.apache.httpcomponents" % "httpclient" % "4.3.3",
  "org.apache.httpcomponents" % "httpmime" % "4.3.3",
  "org.apache.httpcomponents" % "httpcore" % "4.3.3",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.scalatestplus" %% "play" % "1.2.0" % "test",
  "org.easymock" % "easymock" % "3.3.1" % "test"
)
